package model;

public class BankAccount {

	private int id;
	private String name;
	private static double balance = 10000;
	
	public BankAccount(int id,String name){
		this.id = id;
		this.name = name;
		
	}
	
	public void deposit(double amount){
		balance += amount;
	}
	
	public void withDraw(double amount){
		balance -= amount;
	}
	
	public double getBalance(){
		return balance;
	}
}
