package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class view4 extends JFrame{
	
	private static final int FRAME_WIDTH = 450;
	private static final int FRAME_HEIGHT = 200;
	private JPanel background;
	private JPanel panel;
	private JComboBox box;
	
	

	
	public view4(){
		createComboButton();
		createPanel();
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
		setVisible(true);
		setResizable(false);
	} 
	
	private void createComboButton(){
		
		box = new JComboBox();
		box.addItem("Select color");
		box.addItem("RED");
		box.addItem("GREEN");
		box.addItem("BLUE");
		
		box.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				if(box.getSelectedIndex()==1){
					background.setBackground(Color.RED);
					panel.setBackground(Color.RED);
				}
				if(box.getSelectedIndex()==2){
					background.setBackground(Color.GREEN);
					panel.setBackground(Color.GREEN);
				}
				if(box.getSelectedIndex()==3){
					background.setBackground(Color.BLUE);
					panel.setBackground(Color.BLUE);
				}
			}
			});
	}
	
	private void createPanel(){
		background = new JPanel();
		panel = new JPanel();
		background.setLayout(new BorderLayout());
		panel.add(box); 
		background.add(panel,BorderLayout.SOUTH);
		add(background);
		
	 }
	
}
