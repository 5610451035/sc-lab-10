package view;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.BankAccount;



public class BankAccountLabel extends JFrame {

	private static final int FRAME_WIDTH = 450;
	private static final int FRAME_HEIGHT = 200;
	private JPanel background;
	private JPanel dep_panel;
	private JPanel with_panel;
	private JPanel balance_panel;
	private JMenuBar bar;
	private JMenu menu;
	private JMenu color;
	private JMenuItem red;
	private JMenuItem green;
	private JMenuItem yellow;
	private JLabel dep_label;
	private JLabel with_label;
	private JLabel showBalance;
	private JTextField getdep;
	private JTextField getwith;
	private JButton dep_button;
	private JButton with_button;
	private String str;

	public BankAccountLabel(BankAccount bank){
		createMenuButton();
		createLabel();
		createTextFeild();
		createButton(bank);
		createPanel();
		setResult("Balance :  "+bank.getBalance());
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		setResizable(false);
		setVisible(true);
	}
	
	
	public void createMenuButton(){
		
		bar = new JMenuBar();
		menu = new JMenu("Option");
		color = new JMenu("Color");
		red = new JMenuItem("RED");
		green = new JMenuItem("GREEN");
		yellow = new JMenuItem("YELLOW");
		color.add(red);
		color.add(green);
		color.add(yellow);
		menu.add(color);
		bar.add(menu);
		
		red.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				background.setBackground(Color.RED);
				dep_panel.setBackground(Color.RED);
				with_panel.setBackground(Color.RED);
				balance_panel.setBackground(Color.RED);
			}
			});
		green.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				background.setBackground(Color.GREEN);
				dep_panel.setBackground(Color.GREEN);
				with_panel.setBackground(Color.GREEN);
				balance_panel.setBackground(Color.GREEN);
			}
			});
		yellow.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				background.setBackground(Color.YELLOW);
				dep_panel.setBackground(Color.YELLOW);
				with_panel.setBackground(Color.YELLOW);
				balance_panel.setBackground(Color.YELLOW);
			}
			});
				
	}
	
	public void createLabel(){
		
		dep_label = new JLabel("Deposit : ");
		with_label = new JLabel("Withdraw : ");
		showBalance = new JLabel("Balance : ");
	}
	
	public void createTextFeild(){
		
		getdep = new JTextField(20);
		getwith  = new JTextField(20);
		
	}
	 
	public void createButton(BankAccount bank){
		dep_button = new JButton("OK");
		with_button = new JButton("OK");
		
		dep_button.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				 double amount = Double.parseDouble(getdep.getText());
		           bank.deposit(amount);
		           showBalance.setText("Balance :  "+bank.getBalance());
			}
			});
		with_button.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				 double amount = Double.parseDouble(getwith.getText());
		           bank.withDraw(amount);
		           showBalance.setText("Balance :  "+bank.getBalance());
			}
			});   
	}
	
	public void createPanel(){
		background = new JPanel();
		dep_panel = new JPanel();
		with_panel = new JPanel();
		balance_panel = new JPanel();
		background.setLayout(new GridLayout(3,1));
		dep_panel.add(dep_label);
		dep_panel.add(getdep);
		dep_panel.add(dep_button);
		with_panel.add(with_label);
		with_panel.add(getwith);
		with_panel.add(with_button);
		balance_panel.add(showBalance);
		background.add(dep_panel);
		background.add(with_panel);
		background.add(balance_panel);
		setJMenuBar(bar);
		add(background);
		
	}
	
	public void setResult(String str) {
		this.str = str;
		showBalance.setText(this.str);
	}
	
	
	   

}