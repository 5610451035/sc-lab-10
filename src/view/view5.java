package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;


public class view5 extends JFrame{
	
	private static final int FRAME_WIDTH = 450;
	private static final int FRAME_HEIGHT = 200;
	private JPanel background;
	private JPanel panel;
	private JMenuBar bar;
	private JMenu menu;
	private JMenu color;
	private JMenuItem red;
	private JMenuItem green;
	private JMenuItem blue;
	
	

	
	public view5(){
		createMenuButton();
		createPanel();
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
		setVisible(true);
		setResizable(false);
	} 
	
	private void createMenuButton(){
		
		bar = new JMenuBar();
		menu = new JMenu("Option");
		color = new JMenu("Color");
		red = new JMenuItem("RED");
		green = new JMenuItem("GREEN");
		blue = new JMenuItem("BLUE");
		color.add(red);
		color.add(green);
		color.add(blue);
		menu.add(color);
		bar.add(menu);
		
		red.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				background.setBackground(Color.RED);
				panel.setBackground(Color.RED);
			}
			});
		green.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				background.setBackground(Color.GREEN);
				panel.setBackground(Color.GREEN);
			}
			});
		blue.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				background.setBackground(Color.BLUE);
				panel.setBackground(Color.BLUE);
			}
			});
				
	}
	
	private void createPanel(){
		background = new JPanel();
		panel = new JPanel();
		background.setLayout(new BorderLayout());
		background.add(panel,BorderLayout.SOUTH);
		setJMenuBar(bar);
		add(background);
		
	 }
	
}
