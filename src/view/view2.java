package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;


public class view2 extends JFrame{
	
	private static final int FRAME_WIDTH = 450;
	private static final int FRAME_HEIGHT = 200;
	private JPanel background;
	private JPanel panel;
	private JRadioButton red_redio;
	private JRadioButton green_redio;
	private JRadioButton blue_redio;
	private ButtonGroup group;

	
	public view2(){
		createRadioButton();
		createPanel();
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
		setVisible(true);
		setResizable(false);
	} 
	
	private void createRadioButton(){
		
		red_redio = new JRadioButton("RED");
		green_redio = new JRadioButton("GREEN");
		blue_redio = new JRadioButton("BLUE");
		
		group = new ButtonGroup();
		group.add(red_redio);
		group.add(green_redio);
		group.add(blue_redio);
		
		red_redio.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				background.setBackground(Color.RED);
				panel.setBackground(Color.RED);
				}
			});
		green_redio.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				background.setBackground(Color.GREEN);
				panel.setBackground(Color.GREEN);
				}
			});
		blue_redio.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				background.setBackground(Color.BLUE);
				panel.setBackground(Color.BLUE);
				}
			});
	}
	
	private void createPanel(){
		background = new JPanel();
		panel = new JPanel();
		background.setLayout(new BorderLayout());
		panel.add(red_redio); 
		panel.add(green_redio); 
		panel.add(blue_redio); 
		background.add(panel,BorderLayout.SOUTH);
		add(background);
		
	 }
	
}
